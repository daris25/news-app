import Intro from '../screens/general/Login'
import SignUp from '../screens/general/SignUp'
import SignIn from '../screens/general/SignIn'
import Tabs from  '../screens/general/Tabs'
import Category from '../screens/general/Category'
import Sobook from '../screens/general/Sobook'

export {
    Tabs,
    Intro,
    SignIn,
    SignUp,
    Category,
    Sobook
};
