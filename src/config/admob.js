import admob, {MaxAdContentRating} from '@react-native-firebase/admob';

export function configAdmob(params) {
  admob()
    .setRequestConfiguration({
      // Update all future requests suitable for parental guidance
      maxAdContentRating: MaxAdContentRating.PG,

      // Indicates that you want your content treated as child-directed for purposes of COPPA.
      tagForChildDirectedTreatment: true,

      // Indicates that you want the ad request to be handled in a
      // manner suitable for users under the age of consent.
      tagForUnderAgeOfConsent: true,
    })
    .then((res) => {
      console.log('request config', res);
      // Request config successfully set!
    });
}
