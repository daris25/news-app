import {put, call, takeLatest, all} from 'redux-saga/effects';

import {handleError} from '../helper';
import {
  POST_LOGIN_BEGIN,
  POST_LOGIN_FAILURE,
  POST_LOGIN_SUCCESS,
  POST_LOGOUT_BEGIN,
  POST_LOGIN_REQUEST,
  POST_LOGOUT_REQUEST,
  POST_LOGOUT_SUCCESS,
  POST_LOGOUT_FAILURE,
} from './constant';

import {loginUser} from '../../api/user'

export function* postLogin({payload}) {
  console.log('postLogin payload', payload.form.user);
  try {
    yield put({type: POST_LOGIN_BEGIN});
    console.log('data payload saga',payload);
    const data = yield call(loginUser, payload.form);
    console.log('data user',data.data.result[0]);
    yield put({
      type: POST_LOGIN_SUCCESS,
      payload: data.data.result[0],
    });
    
  } catch (error) {
    console.log(error);
    yield call(handleError, error, POST_LOGIN_FAILURE);
  }
}

export function* postLogout({payload}) {
  try {
    yield put({type: POST_LOGOUT_BEGIN});
    // Once user loggedout from server
    yield put({
      type: POST_LOGOUT_SUCCESS,
      payload: ''
    });
  } catch (error) {
    yield call(handleError, error, POST_LOGOUT_FAILURE);
  }
}

export default function* authFetch() {
  yield all([
    takeLatest(POST_LOGIN_REQUEST, postLogin),
    takeLatest(POST_LOGOUT_REQUEST, postLogout),
  ]);
}
