import {base_url as http} from './http'

export const followingGet = data => {
    console.log(data)
    return http.get('/Follow/following?email='+data)
}

export const followerGet = data => {
    console.log(data)
    return http.get('/Follow/followers?email='+data)
}