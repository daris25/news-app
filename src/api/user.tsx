import {base_url as http} from './http'

export const register = data => {
    console.log(data)
    return http.post('/User/registrasi', data)
}

export const loginUser = data => {
    console.log(data)
    return http.post('/User/login', data)
}

export const profileUser = data =>{
    console.log(data)
    return http.get('/User/profileUser?email='+data)
}