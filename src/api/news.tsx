// const NewsAPI = require('newsapi');
// const newsapi = new NewsAPI('5517bb9dde79424bbd7a20bfdf49163e');

// export function TopHeadlines(params){
//     // To query /v2/top-headlines
// // All options passed to topHeadlines are optional, but you need to include at least one of them
//     newsapi.v2.topHeadlines({
//         // sources: 'bbc-news,the-verge',
//         // q: 'bitcoin',
//         // category: 'business',
//         // language: 'id',
//         country: 'id'
//     }).then(response => {
//         console.log(response);
//     });
// }

// export function Everything(params){
//   // To query /v2/everything
//   // You must include at least one q, source, or domain
//     newsapi.v2.everything({
//         q: 'bitcoin',
//         sources: 'bbc-news,the-verge',
//         domains: 'bbc.co.uk, techcrunch.com',
//         from: '2017-12-01',
//         to: '2017-12-12',
//         language: 'id',
//         sortBy: 'relevancy',
//         page: 2
//     }).then(response => {
//         console.log(response);
//     });
// }

// export function Source(params){
//     // To query sources
//     // All options are optional
//     newsapi.v2.sources({
//         category: 'technology',
//         // language: 'id',
//         country: 'id'
//     }).then(response => {
//         console.log(response);
//     });
// }


// // category
// // The category you want to get headlines for. 
// // Possible options: business, entertainment, general, health, science, sports, technology . 
// // Note: you can't mix this param with the sources param.

import {base_url as http} from './http'

export const newsNew = data =>{
    console.log(data)
    return http.get('/News/newsNew');
}

export const newsPopular = data =>{
    return http.get('/Offer/popularNews')
}

export const activityNews = data =>{
    console.log(data)
    return http.get('/Activity/activityLine?email='+data)
}

export const activitySelf = data =>{
    console.log(data)
    return http.get('/Activity/activitySelf?email='+data)
}

export const addComment = data =>{
    console.log(data)
    return http.post('/Comment/addComment')
}

export const getComment = data=>{
    console.log(data)
    return http.get('/Comment/getComment?id_news='+data)
}