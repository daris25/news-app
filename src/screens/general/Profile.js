/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import img from '../../config/images';
import {activitySelf} from '../../api/news';
import {profileUser} from '../../api/user';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {followerGet, followingGet} from '../../api/follow';

export default function Profile(props) {
  const user = useSelector((state) => state.auth.data);
  const width = 100 / 3;
  const [post, setPost] = useState('');
  const [profile, setProfile] = useState('');
  const [follow, setFollow] = useState('');

  useEffect(() => {
    pushAPI();
  }, []);

  async function pushAPI(params) {
    const news = await activitySelf(user.email);
    const profile = await profileUser(user.email);
    const fow = await followerGet(user.email);
    const fol = await followingGet(user.email);
    // console.log('cek follow', fow.data.result[0])
    const foll = await {
      title: news.data.count[0].count,
      followers: fow.data.result[0].count,
      following: fol.data.result[0].count,
    };
    console.log('fo', foll);
    console.log('news : ', news.data);
    console.log('profile : ', profile.data.result[0]);
    setFollow(foll);
    setPost(news.data);
    setProfile(profile.data.result[0]);
  }

  return (
    <SafeAreaView>
      {post !== '' ? (
        <View style={{height: '100%', width: '100%'}}>
          <View
            style={{
              justifyContent: 'flex-end',
              flexDirection: 'row',
              padding: 15,
            }}>
            <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
              <FontAwesome name="bars" size={25} color="grey" />
            </TouchableOpacity>
          </View>
          <ScrollView>
            <View style={styles.boxName}>
              <View style={styles.boxImg}>
                <Image
                  source={
                    profile.image == null || profile.image == ''
                      ? img.avatar
                      : {uri: profile.image}
                  }
                  style={{width: 125, height: 125, borderRadius: 100}}
                />
              </View>

              <View
                style={{
                  //   justifyContent: 'flex-end',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  margin: 20,
                  marginTop: 90,
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                  {profile.name}
                </Text>
                <Text
                  style={{fontSize: 14, textAlign: 'center', marginTop: 10}}>
                  {profile.description}
                </Text>
              </View>
              {/*     
                        <TouchableOpacity 
                            style={{width: 100, height: 30, backgroundColor: 'green', borderRadius: 3, alignSelf: 'center', 
                                justifyContent: 'center', alignItems: 'center'
                            }}
                        >
                            <Text style={{fontSize: 13, color: 'white'}}>Follow</Text>
                        </TouchableOpacity>

                        <TouchableOpacity 
                            style={{width: 100, height: 30, backgroundColor: 'maroon', borderRadius: 3, alignSelf: 'center', 
                                justifyContent: 'center', alignItems: 'center'
                            }}
                        >
                            <Text style={{fontSize: 13, color: 'white'}}>Unfollow</Text>
                        </TouchableOpacity> */}

              <View
                style={{
                  width: '100%',
                  height: 60,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View style={[styles.boxInfo, {width: width + '%'}]}>
                  <Text style={{fontWeight: 'bold', fontSize: 17}}>
                    {post.count[0].count}
                  </Text>
                  <Text style={{fontSize: 14, color: 'grey'}}>Title</Text>
                </View>
                <View style={[styles.boxInfo, {width: width + '%'}]}>
                  <Text style={{fontWeight: 'bold', fontSize: 17}}>
                    {follow.followers}
                  </Text>
                  <Text style={{fontSize: 14, color: 'grey'}}>Followers</Text>
                </View>
                <View style={[styles.boxInfo, {width: width + '%'}]}>
                  <Text style={{fontWeight: 'bold', fontSize: 17}}>
                    {follow.following}
                  </Text>
                  <Text style={{fontSize: 14, color: 'grey'}}>Following</Text>
                </View>
              </View>
            </View>

            <View>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  marginBottom: 10,
                  marginLeft: 5,
                  marginTop: 10,
                }}>
                Articles
              </Text>
              {post.status !== 202
                ? post.result.map((art) => {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          props.navigation.navigate('Sobook', {post: art})
                        }
                        key={art.id}
                        style={{
                          flexDirection: 'row',
                          borderBottomWidth: 0.5,
                          borderBottomColor: 'grey',
                        }}>
                        <Image
                          style={{width: 70, height: 70}}
                          source={{uri: art.image}}
                        />
                        <View style={{padding: 10}}>
                          <Text style={{fontSize: 16, color: 'black'}}>
                            {art.title}
                          </Text>
                          <View style={{flexDirection: 'row', marginTop: 5}}>
                            <Text
                              style={{
                                marginRight: 10,
                                fontSize: 13,
                                color: 'grey',
                              }}>
                              200 likes
                            </Text>
                            <Text
                              style={{
                                marginRight: 10,
                                fontSize: 13,
                                color: 'grey',
                              }}>
                              200 riview
                            </Text>
                            <Text
                              style={{
                                marginRight: 10,
                                fontSize: 13,
                                color: 'grey',
                              }}>
                              {art.publish}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    );
                  })
                : null}
            </View>

            {/* <View style={{marginRight: 5, marginLeft: 5, marginTop: 10}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold', marginBottom: 10}}>Interest</Text>
                        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                            <View style={styles.interestView}>
                                <Text style={{color: 'white'}}>Music</Text>
                            </View>
                            <View style={styles.interestView}>
                                <Text style={{color: 'white'}}>Volly</Text>
                            </View>
                            <View style={styles.interestView}>
                                <Text style={{color: 'white'}}>Sing</Text>
                            </View>
                            <View style={styles.interestView}>
                                <Text style={{color: 'white'}}>Vacansy</Text>
                            </View>
                            <View style={styles.interestView}>
                                <Text style={{color: 'white'}}>Band</Text>
                            </View>
                            <View style={styles.interestView}>
                                <Text style={{color: 'white'}}>Regea</Text>
                            </View>
                        </View>
                    </View> */}
          </ScrollView>
        </View>
      ) : null}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  boxImg: {
    width: 130,
    height: 130,
    backgroundColor: 'lightgrey',
    borderRadius: 100,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -50,
  },
  boxName: {
    width: '90%',
    // height: 225,
    backgroundColor: 'white',
    alignSelf: 'center',
    marginTop: 55,
    // marginBottom: 10,
    borderRadius: 10,
    justifyContent: 'flex-end',

    shadowOffset: {width: 1, height: 1},
    shadowColor: 'lightgrey',
    shadowOpacity: 1,
  },
  boxInfo: {
    height: '100%',
    backgroundColor: 'white',
    // borderWidth: 0.17,
    // borderColor: 'lightgrey',
    alignItems: 'center',
    justifyContent: 'center',
  },
  interestView: {
    backgroundColor: 'green',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 20,
    marginBottom: 5,
    marginRight: 5,
  },
});
