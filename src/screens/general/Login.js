import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView>
        <View style={styles.headView}>
          <Text style={styles.titleText}>Intro</Text>
          <Text style={styles.subText}>
            {' '}
            Dicovery blog earlyby sweeping from and category to another{' '}
          </Text>
        </View>

        <ScrollView horizontal={true}>
          <Image
            source={{
              uri:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKxW0oIrEMD96n5WGIb8DNVx82-EwIQ2FYznYqWwhBVSZE3o01&s',
            }}
            style={styles.imageView}
          />
          <Image
            source={{
              uri:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKxW0oIrEMD96n5WGIb8DNVx82-EwIQ2FYznYqWwhBVSZE3o01&s',
            }}
            style={styles.imageView}
          />
          <Image
            source={{
              uri:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKxW0oIrEMD96n5WGIb8DNVx82-EwIQ2FYznYqWwhBVSZE3o01&s',
            }}
            style={styles.imageView}
          />
        </ScrollView>

        <View>
          <TouchableOpacity style={styles.buttonView}>
            <Text style={styles.textS}>Sign Up</Text>
          </TouchableOpacity>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text>Already have account ?</Text>
            <TouchableOpacity>
              <Text style={{fontWeight: 'bold', fontSize: 15}}> Sign In</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    paddingLeft: 35,
    paddingRight: 35,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 30,
  },
  subText: {
    fontSize: 13,
    textAlign: 'center',
  },

  imageView: {
    height: 300,
    width: 250,
    alignSelf: 'center',
    marginTop: 50,
    marginBottom: 20,
    borderRadius: 5,
    elevation: 5,
    marginRight: 10,
  },

  buttonView: {
    marginTop: 25,
    backgroundColor: 'black',
    width: '80%',
    height: 45,
    alignSelf: 'center',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
  textS: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
});
