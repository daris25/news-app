/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Image,
  SafeAreaView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import {useSelector} from 'react-redux';
import {activityNews} from '../../api/news';

export default function Cerpenis(props) {
  const user = useSelector((state) => state.auth.data);
  const [activity, setActivity] = useState('');

  useEffect(() => {
    pushAPI();
  }, []);

  const pushAPI = async () => {
    const ac = await activityNews(user.email);
    setActivity(ac.data.result);
    console.log(ac);
  };

  const logOutZoomState = (event, gestureState, zoomableViewEventObject) => {
    // console.log('Event: ', event);
    // console.log('GestureState: ', gestureState);
    // console.log('ZoomableEventObject: ', zoomableViewEventObject);
    // console.log(`Zoomed from ${zoomableViewEventObject.lastZoomLevel} to  ${zoomableViewEventObject.zoomLevel}`);
  };

  return (
    <SafeAreaView>
      <View style={styles.recomView}>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>Activity</Text>
        <TouchableOpacity>
          <Icon name="search1" style={{fontSize: 30}} />
        </TouchableOpacity>
      </View>
      <ScrollView>
        {activity !== ''
          ? activity.map((act) => {
              var hei = 0;
              var wid = 0;
              Image.getSize(act.image, (height, width) => {
                hei = height;
                wid = width;
              });

              return (
                <TouchableOpacity
                  key={act.id}
                  onPress={() =>
                    props.navigation.navigate('Sobook', {post: act})
                  }
                  style={styles.postView}
                  activeOpacity={1}>
                  <View
                    style={{
                      flexDirection: 'row',
                      padding: 15,
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 50,
                        height: 50,
                        backgroundColor: 'maroon',
                        borderRadius: 100,
                        justifyContent: 'center',
                      }}>
                      <Image
                        source={{
                          uri:
                            'https://img.okezone.com/content/2019/12/24/33/2145602/kaleidoskop-2019-artis-pindah-keyakinan-salmafina-hingga-deddy-corbuzier-7KHG7Pa4G3.jpg',
                        }}
                        style={{
                          width: '100%',
                          height: '100%',
                          borderRadius: 100,
                        }}
                      />
                    </View>
                    <View style={{marginLeft: 15}}>
                      <Text style={{fontSize: 15, fontWeight: 'bold'}}>
                        {act.author}
                      </Text>
                      <Text style={{fontSize: 12}}>{act.title}</Text>
                    </View>
                  </View>
                  <View style={{paddingLeft: 15, paddingRight: 15}}>
                    <Text
                      style={{
                        fontSize: 14,
                        textAlign: 'justify',
                        marginBottom: 10,
                      }}
                      numberOfLines={4}>
                      {act.content}
                    </Text>
                    <ReactNativeZoomableView
                      maxZoom={1.5}
                      minZoom={0.5}
                      zoomStep={0.5}
                      initialZoom={1}
                      bindToBorders={true}
                      onZoomAfter={logOutZoomState()}
                      // style={{
                      //   padding: 10,
                      //   backgroundColor: 'red',
                      // }}
                    >
                      <Image
                        source={{uri: act.image}}
                        style={{
                          aspectRatio: hei < wid ? 4 / 3 : 15 / 18,
                          marginBottom: 15,
                          width: '100%',
                        }}
                      />
                    </ReactNativeZoomableView>
                  </View>
                </TouchableOpacity>
              );
            })
          : null}
        <View style={{marginBottom: 75}} />
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  recomView: {
    width: '100%',
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  postView: {
    backgroundColor: 'lightgrey',
    width: '93%',
    // height: 200,
    alignSelf: 'center',
    marginTop: 15,
  },
});
