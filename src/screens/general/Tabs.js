import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';

import HomeScreen from './Home';
import CerpenScreen from './Cerpenis';
import ProfileScreen from './Profile';
import CustomDrawerContent from '../../component/drawer';

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function Draw() {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="User" component={ProfileScreen} />
    </Drawer.Navigator>
  );
}

export default function App() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused ? 'ios-home' : 'ios-home';
          } else if (route.name === 'Profile') {
            iconName = focused ? 'ios-list-box' : 'ios-list';
          } else if (route.name === 'Cerpen') {
            iconName = focused ? 'ios-book' : 'ios-book';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Cerpen" component={CerpenScreen} />
      <Tab.Screen name="Profile" component={Draw} />
    </Tab.Navigator>
  );
}
