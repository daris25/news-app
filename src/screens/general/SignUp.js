/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-undef */
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  TextInput,
  ScrollView,
} from 'react-native';
import {postLoginRequest} from '../../store/auth/action';
import {useDispatch, useSelector} from 'react-redux';

export default function SignUp(props) {
  useSelector((state) => {
    if (state.auth.data !== [] && state.auth.isLoggedIn === true) {
      props.navigation.reset({
        index: 1,
        routes: [{name: 'Tabs'}],
      });
    }
  });
  const dispath = useDispatch();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');

  const regis = async () => {
    if (name === '') {
      Alert.alert('Your Name', 'Your name doesnt empty ..');
    } else if (email === '') {
      Alert.alert('Email', 'Email doesnt empty ..');
    } else if (phone === '') {
      Alert.alert('Phone', 'Your phone doesnt empty ..');
    } else if (password === '') {
      Alert.alert('Password', 'Your password doesnt empty ..');
    } else {
      const reg = await register({
        email: email,
        password: password,
        phone: phone,
        name: name,
      });
      if (reg) {
        dispath(postLoginRequest({email: email, password: password}));
      }
    }
  };

  return (
    <SafeAreaView>
      <View style={styles.headerView}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Text style={styles.headerText}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => props.navigation.navigate('SignIn')}>
          <Text style={styles.headerText}>SignIn</Text>
        </TouchableOpacity>
      </View>
      <ScrollView style={{width: '100%', height: '90%'}}>
        <View style={styles.midView}>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'black',
              fontSize: 25,
              marginBottom: 20,
              marginTop: 20,
            }}>
            Create Your Account
          </Text>
        </View>

        <View
          style={{
            padding: 20,
            height: 320,
            width: '100%',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>
          <View style={styles.inputForm}>
            <TextInput
              style={styles.textForm}
              placeholder="Your Name"
              placeholderTextColor="white"
              autoCapitalize="none"
              onChangeText={(text) => setName(text)}
            />
          </View>
          <View style={styles.inputForm}>
            <TextInput
              style={styles.textForm}
              placeholder="Email"
              placeholderTextColor="white"
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={(text) => setEmail(text)}
            />
          </View>
          <View style={{...styles.inputForm, flexDirection: 'row'}}>
            <View style={{paddingTop: 13, width: 50, height: '100%'}}>
              <Text style={styles.textForm}>+62</Text>
            </View>
            <TextInput
              style={styles.textForm}
              placeholder="Phone Number"
              placeholderTextColor="white"
              keyboardType="phone-pad"
              onChangeText={(text) => setPhone(text)}
            />
          </View>
          <View style={styles.inputForm}>
            <TextInput
              style={styles.textForm}
              placeholder="Password"
              placeholderTextColor="white"
              secureTextEntry={true}
              onChangeText={(text) => setPassword(text)}
            />
          </View>
          <TouchableOpacity style={styles.logButton} onPress={() => regis()}>
            <Text style={{color: 'white', fontSize: 15, fontWeight: 'bold'}}>
              SignUp
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  headerView: {
    paddingLeft: 25,
    paddingRight: 25,
    marginTop: 15,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: 15,
    fontWeight: 'bold',
  },

  midView: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageBlok: {
    width: 130,
    height: 130,
    backgroundColor: 'grey',
    marginTop: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },

  inputForm: {
    width: '100%',
    height: 45,
    backgroundColor: 'grey',
    borderRadius: 10,
    elevation: 4,
  },
  textForm: {
    width: '100%',
    height: '100%',
    marginLeft: 15,
    marginRight: 15,
    color: 'white',
  },
  logButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    height: 45,
    borderRadius: 10,
    marginTop: 20,
  },
});
