/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import ImageView from 'react-native-image-view';
import Review from '../../component/review';

export default function Sobook(props) {
  console.log('props detail cerprn', props.route.params.post);
  const [post, setPost] = useState(props.route.params.post);

  const [image, setImage] = useState('');
  const [visimg, setVisimg] = useState(false);
  const images = [
    {
      source: {
        uri: image.url,
      },
      title: 'Paris',
      width: image.width,
      height: image.height,
    },
  ];

  const goBack = () => {
    props.navigation.goBack();
  };

  return (
    <ImageBackground
      style={{width: '100%', height: '100%'}}
      source={{uri: post.image}}>
      <View style={styles.layerView}>
        {/* <ImageView
          images={images}
          imageIndex={0}
          isVisible={visimg}
          renderFooter={(currentImage) => (
            <View>
              <Text>My footer</Text>
            </View>
          )}
        /> */}
        <SafeAreaView>
          <View style={styles.headerView}>
            <TouchableOpacity
              onPress={() => goBack()}
              style={{
                paddingLeft: 10,
                paddingRight: 10,
                paddingTop: 5,
                paddingBottom: 5,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
              }}>
              <Text style={{fontSize: 14, fontWeight: 'bold'}}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                paddingLeft: 10,
                paddingRight: 10,
                paddingTop: 5,
                paddingBottom: 5,
                backgroundColor: 'white',
                borderRadius: 10,
                elevation: 5,
              }}>
              <Text style={{fontSize: 14, fontWeight: 'bold'}}>Bookmark</Text>
            </TouchableOpacity>
          </View>
          <ScrollView style={{width: '100%', height: '90%'}}>
            <View style={styles.contentView}>
              {/* <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold', marginBottom: 10}}>Category</Text> */}
              <Text
                style={{
                  fontSize: 20,
                  color: 'white',
                  fontWeight: 'bold',
                  marginBottom: 10,
                }}>
                {post.title}
              </Text>
              <Text
                style={{fontSize: 14, color: 'white', textAlign: 'justify'}}>
                {post.content}
              </Text>
            </View>
          </ScrollView>
        </SafeAreaView>
        <Review idNews={post.id_news} />
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  headerView: {
    width: '100%',
    height: 50,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  contentView: {
    width: '100%',
    height: '100%',
    // backgroundColor: 'white',
    marginTop: 10,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    padding: 20,
  },

  layerView: {
    backgroundColor: 'rgba(51,51,51,0.6)',
    width: '100%',
    height: '100%',
  },
});
