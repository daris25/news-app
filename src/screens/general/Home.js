/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Button,
} from 'react-native';
import img from '../../config/images';
import AdsBanner from '../../component/adsBanner';
import AdsReward from '../../component/adsRewarded';
import {useSelector} from 'react-redux';
import {profileUser} from '../../api/user';
import {newsNew, newsPopular} from '../../api/news';

export default function Home(props) {
  const user = useSelector((state) => state.auth.data);
  const [newPost, setNewPost] = useState('');
  const [profile, setProfile] = useState('');
  const [popular, setPopular] = useState('');

  useEffect(() => {
    pushAPI();
  }, []);

  async function pushAPI() {
    const prof = await profileUser(user.email);
    const postnew = await newsNew();
    const postPop = await newsPopular();
    setPopular(postPop.data.result);
    setNewPost(postnew.data.result);
    setProfile(prof.data.result[0]);
  }

  return (
    <SafeAreaView>
      <View style={{paddingLeft: 10, paddingRight: 10}}>
        <View style={styles.headView}>
          <Text style={styles.logoText}>meread</Text>
          <Image
            source={profile.photo == '' ? img.avatar : {uri: profile.photo}}
            style={{width: 50, height: 50, borderRadius: 100}}
          />
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.campImg}>
            <ScrollView horizontal={true}>
              {newPost !== ''
                ? newPost.map((post) => {
                    return (
                      <TouchableOpacity
                        key={post.id}
                        onPress={() =>
                          props.navigation.navigate('Sobook', {post: post})
                        }>
                        <ImageBackground
                          source={{uri: post.image}}
                          style={styles.imgScr}>
                          <View
                            style={{
                              width: '100%',
                              padding: 10,
                              backgroundColor: 'lightgrey',
                              opacity: 0.8,
                              borderTopLeftRadius: 5,
                              borderTopRightRadius: 5,
                            }}>
                            <Text
                              style={{
                                fontSize: 23,
                                fontWeight: 'bold',
                                color: 'white',
                              }}>
                              {post.title}
                            </Text>
                          </View>
                        </ImageBackground>
                      </TouchableOpacity>
                    );
                  })
                : null}
            </ScrollView>
          </View>

          <AdsBanner
            // result={(res) => console.log('cek banner', res)}
            {...props}
          />

          <View style={styles.headView}>
            <Text style={styles.logoText}>Popular</Text>
            <TouchableOpacity
              onPress={() => props.navigation.navigate('Category')}>
              <Text>Show All</Text>
            </TouchableOpacity>
          </View>
          {popular !== ''
            ? popular.map((pop) => {
                return (
                  <TouchableOpacity
                    style={styles.newPop}
                    key={pop.id_news}
                    onPress={() =>
                      props.navigation.navigate('Sobook', {post: pop})
                    }>
                    <View style={{paddingRight: 10}}>
                      <Image
                        source={
                          pop.image === ''
                            ? require('../../asset/images/coverbook.jpeg')
                            : {uri: pop.image}
                        }
                        style={styles.popImg}
                      />
                    </View>
                    <View style={{width: '75%'}}>
                      {/* <Text style={styles.popText1}>Category</Text> */}
                      <Text style={styles.popText2}>{pop.title}</Text>
                      <Text
                        numberOfLines={2}
                        style={{fontSize: 14, color: 'black'}}>
                        {pop.content}
                      </Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={{fontSize: 13, marginRight: 10}}>
                          {pop.review} review
                        </Text>
                        {/* <Text style={{fontSize: 13}}>100 Like</Text> */}
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })
            : null}

          <View style={{marginBottom: 140}} />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  headView: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  logoText: {
    fontSize: 25,
    fontWeight: 'bold',
  },

  campImg: {
    // backgroundColor: 'grey',
    width: '100%',
    height: 400,
    marginTop: 5,
  },
  imgScr: {
    width: 300,
    height: '98%',
    marginRight: 10,
    borderRadius: 10,
    alignSelf: 'center',
    resizeMode: 'stretch',
  },

  newPop: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  popImg: {
    width: 80,
    height: 80,
  },
  popText1: {
    fontSize: 13,
    fontStyle: 'italic',
    marginBottom: 5,
  },
  popText2: {
    fontSize: 17,
    fontWeight: 'bold',
    marginBottom: 5,
  },
});
