import React, {PureComponent} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default function Category(props) {
  var newWeek = [];
  for (let i = 0; i < 5; i++) {
    newWeek.push(
      <TouchableOpacity style={styles.newPop} key={i}>
        <View style={{paddingRight: 10}}>
          <Image
            source={{
              uri:
                'https://marketplace.canva.com/EADapJM06aM/1/0/256w/canva-colorful-pattern-background-creative-wattpad-book-cover-2i-lcOA4LUY.jpg',
            }}
            style={styles.popImg}
          />
        </View>
        <View style={{width: '75%'}}>
          <Text style={styles.popText2}>
            Judul - Chapter 1 kah kdlaklw d a k l jdkl w adkla dlakwd kladkla
            wdkla jkldwjald klajwdlka wd
          </Text>
        </View>
      </TouchableOpacity>,
    );
  }

  return (
    <SafeAreaView>
      <View style={styles.recomView}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Icon name="arrowleft" size={28} />
        </TouchableOpacity>
        <Text style={{fontWeight: 'bold', fontSize: 19}}>Newest this week</Text>
      </View>
      <ScrollView style={{height: '90%'}}>
        <View style={styles.listView}>{newWeek}</View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  recomView: {
    width: '100%',
    // height: 200,
    // backgroundColor: '',
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  boxView: {
    // backgroundColor: 'white',
    width: 100,
    height: 150,
    // borderRadius: 10,
    // marginBottom: 10,
    elevation: 10,
    marginRight: 10,
    justifyContent: 'flex-end',
    padding: 10,
  },
  adsView: {
    width: '100%',
    height: 100,
    backgroundColor: 'grey',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  listView: {
    // backgroundColor: 'maroon',
    width: '100%',
    // height: 200,
    marginBottom: 10,
    padding: 10,
  },

  newPop: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  popImg: {
    width: 80,
    height: 100,
  },
  popText1: {
    fontSize: 13,
    fontStyle: 'italic',
    marginBottom: 5,
  },
  popText2: {
    fontSize: 17,
    fontWeight: 'bold',
    marginBottom: 5,
  },
});
