/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
  ScrollView,
} from 'react-native';
import {postLoginRequest} from '../../store/auth/action';
import {useSelector, useDispatch} from 'react-redux';
import {configAdmob} from '../../config/admob';

export default function SignIn(props) {
  useEffect(() => {
    configAdmob();
  }, []);

  const dispach = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const login = async () => {
    if (email === '') {
      Alert.alert('Email', 'Please inser your email');
    } else if (password === '') {
      Alert.alert('Password', 'Please inser your password');
    } else {
      dispach(
        postLoginRequest({
          email: email,
          password: password,
        }),
      );
    }
  };

  return (
    <SafeAreaView>
      <View style={styles.headerView}>
        <TouchableOpacity onPress={() => props.navigation.navigate('SignUp')}>
          <Text style={styles.headerText}>SignUp</Text>
        </TouchableOpacity>
      </View>

      <ScrollView style={{width: '100%', height: '90%'}}>
        <View style={styles.midView}>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'black',
              fontSize: 20,
              marginBottom: 20,
            }}>
            Login
          </Text>
          <View style={styles.imageBlok}>
            <Image
              source={{
                uri:
                  'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8QDxAPDxAODxAPDw0PDw8PEA8PDw8PFREWFhURFRUYHSggGBomHRUVITEhJSkrLi4uFx8zODMvQygtLisBCgoKDg0OFxAQFy0fHR0tKystLS0rLS0rLS0tLSstLS0tLS0tKy0tLS0tLS0tLS0tLSstLS0tLS0tLS0rLS0tK//AABEIAM4A9QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAABAgADBAUGBwj/xABCEAACAgECAwYDBQUFBgcAAAABAgARAwQhEjFBBQYTIlFhMnGBByNCkaEUUmKCsSSiwfDxU3KSstHhFjM0Q2Nzwv/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACQRAQEAAgICAgIDAQEAAAAAAAABAhEDIRIxBDIzQRMicSMF/9oADAMBAAIRAxEAPwDcJimVjxSxcUvTHPT28nHAiJMhMcfFhsgDmZtcXZRrc7zPLOT26MOO3016Y5Yqzc6fQqo33jvo1PSpl/NG84a1CrGqbX9lUDlMLLiomuUJnKd47GPww8MtXETyBln7O3pH5RPjWOFjgSw4iOkgWGz8dAFh4YwEapJ6JUYCNUZFitVIVVuXpjkRJbUzyyaSABIRGqAyVK2WJUtIi8MApKwcMvqIwj2FTCIVl3DIUhsMZllDY5mskQpK2WmA+CY+TBNsUlL4o/JNxadtPJNk2GSPafBrVSXYsVkAdZdh0pIuZWh0h4rPIGb3KSObHjtsZui0IXc7n1mcBIiw1OLLK13TGSagiGoIZJgRKzjEtMEoihQIahgJgCMsxMuPfaZsVhKxy0nLHbBAjyzIkrmu9s9aGowiBoQd4qcXJLIixxMq1gwQwQMDBUMkAWoCI8BECJUPDGqNUDVFYpSXEQVAKCkrZJlEROGGwxvDhl5EkNlpj6BBUz0WYuiAqZohneyx9CJJIZCgEkMEZDBJJA0iwkwRkhMUmScT3s7c1OXM+g0CtxrwDUZxVYwwvhDHZTR58+dcoD2yO8HfrT6clMQOpyAlTwnhxqR04vxH5fnOI1nf3XvYD48O/wCDELHtbXNh/wCBMtWcwGw6En85g6/udnUbOHrrRBkfzaa/wWsRu+XaJP8A6kj5Jhof3Z0nYf2ii1TV46vbxsXK/wCJCf6H6Tg9Z2TnxXxIduo/rMLDk34T1/r0lTk2yvHq9x9EaHWY8yDJhdciH8Sm9/Q+h9jMoTwbsztfPpXGTE7Idif3Mi1txDkfrPUe6He9NaTjdfDzqC3CLKOt0Sp9RtYPr1lb2PTqZJIQIjCpKjgSVAiQ1CZBAJUEBMgMDGosaAwBWixjK2MADGSITBGB0czRMfEKl4iyu6UmoaQGCSSYyQSXGEgJkJixgbgkgJjJru8faX7Lpc2cfEi0g/jYhV+e5H5TTd29CcOnXi3y5Pvc7n4myvubPWuX0mL9pmprDpsX+01AJHqFU0PzYQdq9vth+7wYWzuo85vhxqR0vqZlyX9NuKft0JTaJ4YM87Tv1qw5D48SgdFNn87M3Ld4cownPwOV35DYTG3TfHtutbpcVENW/OcF2/3WHF4uDlfmX0/iEwdd3x1DE0Er3uNo+9+r6jC49CANvpLmN9s8s8b0OLs9uE4coqkLI3UH0v0vf5Eek0WLO+NrVmRlbyshKlT6gidxj1aapGIHA3DRU9PcH0nCa/E2PIyNzB/z/WaYsOR7f3G7fOt0qs//AJuP7vLuPMw/FXuN50onmH2Nhv7UfwfdDr8W/wDhPTgZdTDSGC4CYjAmAmSQxgJJIIAbgJgMWAQmI0YmVsYwUyRWMkCZSmWAyoCMDEawRriAw3FoDcBMFwRgYJCYtxgbgkkgTivtH0xduzmHIaxcZ/n4SP8Akmu7w9ianUN4aNwYRZYgEu7X8NCrHLr/ANuz7axo5wowsrlGZfmgIv6FhHUbTHk9t+OdV5t2X3HIdeLj4QbYsFth6UDt851vbOFcOiOJB5QCPXbrLu3u28ekxPkILFaFLuSx5Ceddv8Ae3VZGUsGxowDhKAYr/FM53Wt1jFOHuv4rDhOx3O9WJbq+5+RD90pHKm4wf0AE2ndntNC/CCSpRSL5q1m1nWvnWukrd2nwxs3pyPYfZeXE3FkAFgg72D7zlu9S1qXHpVe+09E1WrW+k5jvR2YMz4sinhvyOego7MfzmmNYckdN9jaH9l1DEbHOADtvSCx9L/Weh3OA+zjVeFkfQopOJcLagZSd2ycao1DoP8ApO+lb2nWuqNwxZIwJgkguAGAwXFJgEikyExCYwhMrZpGaVM0chIWklLPJK0W2zUx5jqZapkGcQxbguIzXBcFwXGQyQQwCQxbkgGD2zkCqhrfj2Ppsf8AtMcZvLcyO10Bx8RNBbs8PFQqazxeFa9phye3Vxa1C9oHSY8Q/afDKsbrIA3E3qAZyXbHa/Z2Uur2xYrTcIpQOQEzu1dPjHFmfGdS9cnp1Suiqdh/WcxqNe7KyY9AEvdmOJdx+W0zmq0yum17OGlA+4OMg/u0G+oi67tJhdHl/Sc9ptOA3GR4Tc/KSP0G0zGzcW1fUytdo3ZOyLriW59f6mzNgMgZMqsfjxsFs0ePpX1qaxMFNfvNh2XoM+ozOuLC+YYuC2XIuM42Yc7Yi9pp+umF7vbpfs30+RcrjKWJxYWRSb+FmQ1fX4Z6BNL3Y7E/ZULOby5AoY8RbhVbpb6nc2f9TuqlSdJyu6kkkkZJBITFJjgQmITIxlbNGQlpWzQM0rZpUhIzSl3gd5Q7ypE2ozyTHZpJekbdAywiGoGmDYbhuJclwGzSXFuAmA2e4blXFCDAjw3EuSAWqZzXazDFlIfZXt0PQjqPmD/hOiBmJ2vo8efCyZFDDmOhU+oPQ85GeO4048tVpk1GBF47VrF8xU0ev7yYBdcB3IrqRNN2v3W1CWMeVnS7AbnOR12hzYjWQMPf1+s55jL+3RlyZT9Oj1/aGneyNj7cpqG1q3tymoRSfWZGPTMZrMZGNzuTZ6fVF3A957D3R0Aw6RNqfN99kPUltwPotCeSdkaKj7mhv7z3NVCgAcgAB8gJpEU9yXFuC49Ee4CYlxS0COTEJilopMYEmVsYSYrGOQiMZUxlhiFZcTVDyh5llJU2KUmsJhJMg45JSNN5cBMkWc7cRITBAYBLgJkgMeglxgYlwqYA9wxYQYgYSnWuRjbhqyKF8pbc1D9qYc5y48bB/AyHHkP4PFC8RQHrW11y+mwBVkce45jaxNX2r2bjyg8QHzmVlxCzwlgxDNt5mta2K8yNyLFkg8+RmHqdWqAplyY6biCuSqFhV7i+fCynb1PoZzZ8dnp04csvt5/rdFjXIwSqBqWafEJt9R2SQ1IvEWugP+vSZmk7NXGQHKZHtD5WZlXzPyVVJPwDnXxH03rCWlnZi1+m0bEggcKm/MbtgNqUcz8/nOu0ve7Tac4NLqS+JmQBMzBThoMVCswNqRQBsV7zT4FA+HdvBHFz42AJqxbZD9SOfua5vv2qnHirdlc1y2Wtxtt0Xl6fOdMx1HLlnbdvZeKAmedfZz3uVkXRalwHWl0+Rjs69MRP7w5D1FDmN/QpKt7MTFuC4CY9ASYILkuPQQwVGhhsKysHDLajBYbGlHBIccv4ZGqGxphPhkmQ0ke6XjF0gjEQVJMpikyMYhMNASYLgJi3GDyRZTrtdhwLx58mPEvq7Bb9gOp9hGGTcIM4jtT7RtMljT43zn95vucf6jiP5Ccd2v3212otfE8FD+DB93t7tfF+sXReT0Dvr3rx6TE+LE4bVOCqqu/g3/7jehHQetdJzX2X5jwZ0OwbMrK25LPwEFbv2VuvJtrquCJ6z0DuEvDgxNuOPLkXi84BJ2AseW9+RsnagdgUne66psyheNyvhquZyWKFKHX9z4Qx2KmunOvLO2dVlz6jxsviBV4RhUswKqoFHzElSSAx57k852/fjNm/ZhiwqxZw3iFWJ4MONxxWwO45AhgaBazsZy/aXiZsK1jYZMdjPYChWHuas+wmeeVmnTxceOe9+40up1mRwqs5VU+BVPI/vX1YkA2evynWdgdoNqMNZHDPjAVizjcofK3majYZRfAdwee047R6N8z8C+o4iegJr/Pr0s7Tpuyez30J8cF3BbHibFjDMc1sCV2oA7WPkdjuTWO/bHPW9R0b4iobY8P3y0bC8g90xVfwn8J5n3nF959Rx+bmGoKbFsDfmrnVChy26bidpqUOfhxofJkHFkdFyBnsBmQORxNflB4RtXDXMDie+GQeOqD4VQNwgjyliaXlYIUKN95dvTPTQCbfSd59dirw9Xnr0ZzkUfR7E1QiMAD7yQ7zs/7TNUgrPhw5/wCJScLn51Y/QTe6T7StG22XFqMXqQEyIPqDf6TyYR49nuve+y+2dLqhenzY8hqyoNZAPdDTD8pnEz53TIVIZSVZTYZSQwPqCOU6/sT7QtVhpM4XVYxtbnhzAf74+L6gn3jlPyetBowaaTsTvPo9WFGLKFyNf3GQhMwIFml/F8xY5+hm5jUcGMGlVxWeLQXM8rZ5S2WUvljkLa5skkwmyyStJ8m68S4yykbR1aZ1YOJWRLzvF4YbNURNZ2v21ptIAdRlVOIMUTdneufCo3+vKbcgTwLvP2m2p1mbMTYLsuP0GJSQgH03+ZMe026dT2z9o+Z7XSYxhXkMmSny/MD4V/vTjNVqsmVi+V3yOebuxZj9TMcmEGLaNiTCYkYGII/Kel9z8FaPTna2DPe10dqu+VcNjcb7qLBfzPJynp3dF70mnH8BXrvS3W2+1k7XW5BTzBw46PNv6+YWfjBsDbegQfoK6FeR5F8a5kYLsjOaAoAoPKoFbAUBy2nRdo5KwlhVBLB8h4hwdPwt0PI8weEWrzR6BODEPlMOa9R6Pwse7WLoExLkyYlUL92FJ25lSL3IHUX+u2677HgD5VLC1S0RSwYbjnyAs7Gq5AeVqLry2h4jq8vMWiMNiRYf5EfmDy5GqPVrl4cb5CSSWaqvzAEAqKIDfDuLKgjzEVY2w+kcvPP+uQ+MD4rHljXJzAPFQPOzR5jZuKrFqlqB5T2tnOTM7XYBCLz+FRwivbb0HyE9F7Y1XhaHJlvzNY5ncsSVHKyPNt8OxsGiQ3l55bxsKDNQlIkJswiJJ1hJgEUmBjGuKJDGFul1L4nx5UNPjdcin+JTY/pPftDrUzYsebGbTKiuvyIuj7jl9J89sZ6d9lvaRfTZdOTvgycSj0x5LNf8Qc/zR4nLp3bPKXeVPlmPkzTSQWr3ySjJlmM+omPk1MpncmQ+SSa19TDBPm7A5IVeYQzxxlkeLfyZviSccxQ8dWi8T8ms75dpNp9BqMq7PwDGhHMNkYIG+nFf0nhT9J6X9rHatJh0andz4+X/AHRaoD8zxH+QTzNzJqbTiMIgO0YREkKwSXAC09C7mZL0aWLC5SK33+ECx1+H3NgUpYKZ54Z23cbLeHhH7+ReRNGr5XuaPStrBYUghDjru13+7YCqyY6LfGWF+q7n4vib1vzcR4de60NvSZfa54kxHmQwJs3wk0eZrcgnmOIgkgAFpiJuCT9PlOXn9x6vwfpf9aHGP7cAP9kfXoQd6IPT19+gI3+qYnhQcQB4XZqNvTcKKNuJj6bH0x3Y4ebGS9eCANsZq9+Tr/rfSr3qj0uh8zO1BrVsasfNajc9QDe90QreqDgB6OL6RxfJ/Lk0f2haqseHAp2yMcxAa9hxDkNt2YmwaJBNA2W4LM3T0m975a/xdXlNkjH9ytkk2pPFzAPxFuYv13nPCU5sqIEYQCNESRYTBAzCAGC4RABOj7ga/wAHWqpNLnVsJ9OI+Zf1UD+ac5cKZCrBlNMpDKfRgbB/OOXRPcMmSY2TJH0WoXPhxZl5ZcaPXoSN1+hsfSV5FnRGeTGyOZiZchmc2InpMXNgPpHIyu2vfJvJGyYjckaXRLqJeueabFnmVjyyWszbVM0ycWWapMswe8+v8HQ6hwSGOM41I58WTyAj5cV/SFXK807y9q/teszZwbVn4cf/ANS+VPzAv5kzWNFBkJnOs6chLJVjO0a4Ga4LghiAzou5upCtkxknnjyKoBN78LbdTum2/ThHFwznAZldlajw8yNdA2jXuCD0PtdRy9h6nrs3lxi9gVG2/MKQCTuPiDbXdhrCtwjH1D0hHLaU6vJxDFuOWM7FuhVzdcjuTQ8vm4ifPwivWZdmnN8j7R63wfpf9c8cla3F7sAdieTBrr+W/ar6TqcOo8LTNmJP3aPlYMa4iQ55VQs0N7vkV4qK8azf2vEfc9OLoen+R67XNv3q1fBoAou8mTGgu9gFLGr33oeb4iCQ3vvx/RxfJ/LXCZHJPO/f1PUwCARpTlERgjVdGuXpvIMh9YGyH1MSuh8M8/8AEQFfcfrFuAmBbgmReUSMsCEmLCYtxh6h9mmr8TSPhJ82nyGh/wDHk8wP/Fx/lOt8BZ5V9nOv8LXBLpc+N8ddOIDjU/3WH809SLzXG9F0YqOkxc2MektOSVs8uJumI+lEkud6kjRqNXjSuUysazX4shmZjeSUZiY5x32m9ojhwaVT1OfJ8haoP+c/QTrUczybvPrDm1uoY35cjYgD0XH5P/yT9ZOd6aYtdCTFEBMyUsxmPcqxxxA4eS4oMJMRjchPUc+Yi3CTAnoGn1ofBgyWD5Ap4b5hLIvlsSSU5Cy25ZQG1Obyn5TmuzM5OjdTX3TqV8t7bnc+xckehJO+wG60zcSEn0mPPO5Xqf8An3+uUaTM1ajH68Vfof8AO+3rF7163jyrjF8OEEbknztXFv12Cj6HpUq12T71T6MNvWanNkLszHmxLHoN5phf6uP5X5KAhiwy3Ma4LgkgBggguAEyKYphEQMTFkuAxhdo9S2LLjyr8WJ0yDpZU3X1qp7jgKui5FNq6q6n1VhYP5GeDXPWO42ubJoMIPPHx4vmFPl/QgfSXhQ6F1lR2kbJMbLlmiasaoJhZM8kEbj/2Q==',
              }}
              style={{width: '90%', height: '90%', borderRadius: 100}}
            />
          </View>
        </View>

        <View
          style={{
            padding: 20,
            marginTop: 15,
            height: 210,
            width: '100%',
            justifyContent: 'space-between',
            marginTop: 30,
          }}>
          <View style={styles.inputForm}>
            <TextInput
              style={styles.textForm}
              placeholder="Email"
              placeholderTextColor="white"
              autoCapitalize="none"
              onChangeText={(text) => setEmail(text)}
            />
          </View>
          <View style={styles.inputForm}>
            <TextInput
              style={styles.textForm}
              placeholder="Password"
              placeholderTextColor="white"
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={(text) => setPassword(text)}
            />
          </View>
          <TouchableOpacity onPress={() => login()} style={styles.logButton}>
            <Text style={{color: 'white', fontSize: 15, fontWeight: 'bold'}}>
              Login
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  headerView: {
    paddingLeft: 25,
    paddingRight: 25,
    marginTop: 15,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: 15,
    fontWeight: 'bold',
  },

  midView: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageBlok: {
    width: 130,
    height: 130,
    backgroundColor: 'grey',
    marginTop: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },

  inputForm: {
    width: '100%',
    height: 45,
    backgroundColor: 'grey',
    borderRadius: 10,
    elevation: 4,
  },
  textForm: {
    width: '100%',
    height: '100%',
    marginLeft: 15,
    marginRight: 15,
    color: 'white',
  },
  logButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    height: 45,
    borderRadius: 10,
    marginTop: 20,
  },
});
