/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {postLogoutRequest} from '../store/auth/action';
import {useSelector, useDispatch} from 'react-redux';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {profileUser} from '../api/user';

export default function CustomDrawerContent(props) {
  const dispach = useDispatch();
  const user = useSelector((state) => state.auth.data);
  const [profile, setProfile] = React.useState('');

  React.useEffect(() => {
    apiRender();
  }, []);

  const apiRender = async () => {
    const profile = await profileUser(user.email);
    setProfile(profile.data.result[0]);
  };

  const logout = async () => {
    const log = dispach(postLogoutRequest());
    if (log) {
      props.navigation.reset({
        index: 1,
        routes: [{name: 'Intro'}],
      });
    }
  };

  return (
    <DrawerContentScrollView
      {...props}
      contentContainerStyle={{
        justifyContent: 'space-between',
        width: '100%',
        height: '100%',
        paddingLeft: 15,
        paddingRight: 15,
      }}>
      <View>
        <Text style={{fontSize: 15, fontWeight: 'bold', marginBottom: 10}}>
          {profile.name}
        </Text>

        <TouchableOpacity
          onPress={() => console.log('notig')}
          style={{
            width: '100%',
            height: 40,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <AntDesign name="notification" size={30} />
          <Text style={{fontSize: 15, marginLeft: 15}}>Notification</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            width: '100%',
            height: 40,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <AntDesign name="lock1" size={30} />
          <Text style={{fontSize: 15, marginLeft: 15}}>Security</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            width: '100%',
            height: 40,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Entypo name="eye-with-line" size={30} />
          <Text style={{fontSize: 15, marginLeft: 15}}>Change Password</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            width: '100%',
            height: 40,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <AntDesign name="customerservice" size={30} />
          <Text style={{fontSize: 15, marginLeft: 15}}>Find People</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => logout()}
          style={{
            width: '100%',
            height: 40,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <FontAwesome5 name="comment-dollar" size={30} />
          <Text style={{fontSize: 15, marginLeft: 15}}>Advertise</Text>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity
          style={{
            width: '100%',
            height: 40,
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 20,
          }}>
          <AntDesign name="logout" size={30} />
          <Text style={{fontSize: 15, marginLeft: 15}}>Log Out</Text>
        </TouchableOpacity>
      </View>
    </DrawerContentScrollView>
  );
}
