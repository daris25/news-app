/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
} from 'react-native';
import {activitySelf} from '../api/news';

function Articles(props) {
  const [post, setPost] = useState('');

  useEffect(() => {
    async function pushAPI(params) {
      const news = await activitySelf(user.email);
      setPost(news.data.result);
    }
    pushAPI();
  }, []);

  return (
    <View>
      {post !== ''
        ? post.map((art) => {
            return (
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Sobook', {post: art})}
                key={art.id}
                style={{
                  flexDirection: 'row',
                  borderBottomWidth: 0.5,
                  borderBottomColor: 'grey',
                }}>
                <Image
                  style={{width: 70, height: 70}}
                  source={{uri: art.image}}
                />
                <View style={{padding: 10}}>
                  <Text style={{fontSize: 16, color: 'black'}}>
                    {art.title}
                  </Text>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <Text
                      style={{marginRight: 10, fontSize: 13, color: 'grey'}}>
                      200 likes
                    </Text>
                    <Text
                      style={{marginRight: 10, fontSize: 13, color: 'grey'}}>
                      200 riview
                    </Text>
                    <Text
                      style={{marginRight: 10, fontSize: 13, color: 'grey'}}>
                      {art.publish}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })
        : null}
    </View>
  );
}

export default Articles;
