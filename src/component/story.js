/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  StyleSheet,
} from 'react-native';

function stories(props) {
  return (
    <ScrollView horizontal={true}>
      <View
        style={{
          paddingLeft: 15,
          paddingRight: 15,
          flexDirection: 'row',
          marginBottom: 15,
        }}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: 68,
            height: 80,
          }}>
          <View style={styles.artisView}>
            <Image
              source={{
                uri:
                  'https://img.okezone.com/content/2019/12/24/33/2145602/kaleidoskop-2019-artis-pindah-keyakinan-salmafina-hingga-deddy-corbuzier-7KHG7Pa4G3.jpg',
              }}
              style={styles.artisImg}
            />
          </View>
          <Text>Pitak</Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: 68,
            height: 80,
          }}>
          <View style={styles.artisView}>
            <Image
              source={{
                uri:
                  'https://img.okezone.com/content/2019/12/24/33/2145602/kaleidoskop-2019-artis-pindah-keyakinan-salmafina-hingga-deddy-corbuzier-7KHG7Pa4G3.jpg',
              }}
              style={styles.artisImg}
            />
          </View>
          <Text>Pitak</Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: 68,
            height: 80,
          }}>
          <View style={styles.artisView}>
            <Image
              source={{
                uri:
                  'https://img.okezone.com/content/2019/12/24/33/2145602/kaleidoskop-2019-artis-pindah-keyakinan-salmafina-hingga-deddy-corbuzier-7KHG7Pa4G3.jpg',
              }}
              style={styles.artisImg}
            />
          </View>
          <Text>Pitak</Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: 68,
            height: 80,
          }}>
          <View style={styles.artisView}>
            <Image
              source={{
                uri:
                  'https://img.okezone.com/content/2019/12/24/33/2145602/kaleidoskop-2019-artis-pindah-keyakinan-salmafina-hingga-deddy-corbuzier-7KHG7Pa4G3.jpg',
              }}
              style={styles.artisImg}
            />
          </View>
          <Text>Pitak</Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: 68,
            height: 80,
          }}>
          <View style={styles.artisView}>
            <Image
              source={{
                uri:
                  'https://img.okezone.com/content/2019/12/24/33/2145602/kaleidoskop-2019-artis-pindah-keyakinan-salmafina-hingga-deddy-corbuzier-7KHG7Pa4G3.jpg',
              }}
              style={styles.artisImg}
            />
          </View>
          <Text>Pitak</Text>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  artisView: {
    width: 60,
    height: 60,
    borderRadius: 100,
    backgroundColor: 'lightgrey',
    marginTop: 10,
    // marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  artisImg: {
    width: 55,
    height: 55,
    borderRadius: 100,
  },
});

export default stories;
