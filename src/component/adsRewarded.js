import React, {useEffect, useState} from 'react';
import {View, Button} from 'react-native';
import {
  InterstitialAd,
  TestIds,
  AdEventType,
} from '@react-native-firebase/admob';
import Ads from '../../firebase.json';

const adUnitId = Ads['react-native'].admob_ios_app_id; //__DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-xxxxxxxxxxxxx/yyyyyyyyyyyyyy';
const interstitial = InterstitialAd.createForAdRequest(adUnitId, {
  requestNonPersonalizedAdsOnly: true,
  keywords: ['fashion', 'clothing'],
});

function Rewarded(props) {
  // const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const eventListener = interstitial.onAdEvent((type) => {
      if (type === AdEventType.LOADED) {
        // setLoaded(true);
      }
    });
    // Start loading the interstitial straight away
    interstitial.load();
    // Unsubscribe from events on unmount
    return () => {
      eventListener();
    };
  }, []);

  // if (!loaded) {
  //     return null;
  //   }

  return (
    <View>
      <Button
        title="Show Interstitial"
        onPress={() => {
          interstitial.show();
        }}
      />
    </View>
  );
}

export default Rewarded;
