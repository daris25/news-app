import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  InterstitialAd,
  RewardedAd,
  BannerAd,
  TestIds,
  BannerAdSize,
} from '@react-native-firebase/admob';

function Ads(props) {
  // function onLoad(params) {
  //     props.result = true
  //     return true
  // }

  return (
    <View style={styles.adsView}>
      <BannerAd
        unitId={TestIds.BANNER}
        size={BannerAdSize.BANNER}
        requestOptions={{
          requestNonPersonalizedAdsOnly: true,
        }}
        onAdLoaded={(res) => {
          console.log('onLoaded Ads');
          // props.result = true
        }}
        // onAdOpened={()=> console.log('onAdOpened true')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  adsView: {
    width: '100%',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Ads;
