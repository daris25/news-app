/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
// import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import img from '../config/images';
import {addComment, getComment} from '../api/news';
import {profileUser} from '../api/user';
import {useSelector} from 'react-redux';

export default function Bottom(props) {
  console.log('props bootm sheet', props);
  const user = useSelector((state) => state.auth.data);
  console.log('user review', user);

  const sheetRef = React.useRef(0);
  const [comment, setComment] = React.useState('');
  const [profile, setProfile] = React.useState('');
  const [repli, setRepli] = React.useState('');
  const [refresh, setRefresh] = React.useState(false);
  // const [replys, setReplys] = React.useState(999999999)

  React.useEffect(() => {
    api();
  }, []);

  async function api() {
    const profil = await profileUser(user.email);
    const get = await getComment(props.idNews);
    setComment(get.data.result);
    setProfile(profil.data.result[0]);
    // console.log('cek get comment', get.data.result)
    // console.log('cek get profile', profil.data.result[0])
  }

  const Write = ({params}) => {
    // console.log('cek by comment', params)
    const by =
      params == '' || params == undefined
        ? 'Comment by ' + profile.name
        : 'Reply comment ' + params.name;

    return (
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <View
          style={{
            width: 40,
            height: 40,
            backgroundColor: 'lightgrey',
            borderRadius: 100,
          }}>
          <Image
            source={profile.photo == '' ? img.avatar : img.avatar}
            style={{width: '100%', height: '100%'}}
          />
        </View>
        <View style={{width: '75%', marginRight: 25}}>
          <TextInput
            multiline={true}
            style={{
              width: '100%',
              borderBottomColor: 'grey',
              borderBottomWidth: 1,
              marginLeft: 15,
              paddingBottom: 9,
              paddingTop: 9,
            }}
            placeholder={by}
          />
        </View>
        <TouchableOpacity>
          <MaterialIcons name="send" size={30} color="grey" />
        </TouchableOpacity>
      </View>
    );
  };

  const renderContent = () => (
    <View
      style={{
        backgroundColor: 'white',
        // padding: 16,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 15,
        paddingTop: 5,
        height: '100%',
        width: '100%',
        // alignItems: 'center'
      }}>
      <View style={{alignItems: 'center', marginBottom: 10}}>
        <Text style={{fontSize: 15, fontWeight: 'bold'}}>Swipe up or down</Text>
      </View>

      {repli !== '' ? (
        <View>
          <View
            style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => setRepli('')}
              style={{marginRight: 10}}>
              <AntDesign name="close" size={25} color="black" />
            </TouchableOpacity>
            <Text>Commnet to {repli.name}</Text>
          </View>
          <Write params={repli} />
        </View>
      ) : (
        <Write params={repli} />
      )}

      <ScrollView style={{flex: 1, width: '100%', height: '100%'}}>
        {comment.length !== 0
          ? comment.map((data) => {
              return (
                <View key={data.id_comment}>
                  <View style={{flexDirection: 'row', marginTop: 15}}>
                    <View
                      style={{
                        width: 40,
                        height: 40,
                        backgroundColor: 'lightgrey',
                        borderRadius: 100,
                      }}>
                      <Image
                        source={data.photo === '' ? img.avatar : img.avatar}
                        style={{width: '100%', height: '100%'}}
                      />
                    </View>
                    <View style={{marginLeft: 8, paddingRight: 50}}>
                      <Text
                        style={{
                          color: 'black',
                          fontSize: 13.5,
                          fontWeight: 'bold',
                        }}>
                        {data.name}
                      </Text>
                      <Text style={{color: 'black', fontSize: 13.5}}>
                        {data.comment}
                      </Text>
                      <View style={{flexDirection: 'row', marginTop: 8}}>
                        <Text style={{color: 'grey', fontSize: 13}}>5 Jam</Text>
                        <TouchableOpacity
                          onPress={() => setRepli(data)}
                          style={{marginLeft: 15}}>
                          <Text style={{color: 'grey', fontSize: 13}}>
                            Balas
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                  {data.reply !== undefined
                    ? data.reply.map((reply) => {
                        return (
                          <View>
                            <View
                              style={{
                                flexDirection: 'row',
                                marginLeft: 40,
                                marginTop: 15,
                              }}>
                              <View
                                style={{
                                  width: 40,
                                  height: 40,
                                  backgroundColor: 'lightgrey',
                                  borderRadius: 100,
                                }}>
                                <Image
                                  source={
                                    reply.photo == '' ? img.avatar : img.avatar
                                  }
                                  style={{width: '100%', height: '100%'}}
                                />
                              </View>
                              <View style={{marginLeft: 8, paddingRight: 50}}>
                                <Text
                                  style={{
                                    color: 'black',
                                    fontSize: 13.5,
                                    fontWeight: 'bold',
                                  }}>
                                  {reply.name}
                                </Text>
                                <Text style={{color: 'black', fontSize: 13.5}}>
                                  {reply.comment}
                                </Text>
                                <View
                                  style={{flexDirection: 'row', marginTop: 8}}>
                                  <Text style={{color: 'grey', fontSize: 13}}>
                                    5 Jam
                                  </Text>
                                  <TouchableOpacity
                                    onPress={() => setReplys(reply.id_comment)}
                                    style={{marginLeft: 15}}>
                                    <Text style={{color: 'grey', fontSize: 13}}>
                                      Balas
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              </View>
                            </View>
                          </View>
                        );
                      })
                    : null}
                </View>
              );
            })
          : null}
        <View style={{marginBottom: 20}} />
      </ScrollView>
    </View>
  );

  return (
    <View style={{flex: 2}}>
      <BottomSheet
        ref={sheetRef}
        snapPoints={['5%', '75%']} //['5%', '30%', '55%', '75%']
        borderRadius={10}
        renderContent={renderContent}
      />
    </View>
  );
}
