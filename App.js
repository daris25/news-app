// In App.js in a new project

import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import * as Route from './src/routes';

import {PersistGate} from 'redux-persist/integration/react';
import {Provider, useSelector} from 'react-redux';
import {store, persistor} from './src/store/index';

const Intro = createStackNavigator();
function IntroStack() {
  return (
    <Intro.Navigator headerMode="none">
      <Intro.Screen name="SignIn" component={Route.SignIn} />
      <Intro.Screen name="SignUp" component={Route.SignUp} />
    </Intro.Navigator>
  );
}

const Stack = createStackNavigator();
function Main(props) {
  const user = useSelector((state) => state.auth);
  // console.log('cek user app', user)

  return (
    <Stack.Navigator headerMode="none">
      {user.isLoggedIn === true ? null : (
        <Stack.Screen name="Intro" component={IntroStack} />
      )}
      <Stack.Screen name="Tabs" component={Route.Tabs} />
      <Stack.Screen name="Category" component={Route.Category} />
      <Stack.Screen name="Sobook" component={Route.Sobook} />
    </Stack.Navigator>
  );
}

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <NavigationContainer>
          <Main />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}

export default App;
